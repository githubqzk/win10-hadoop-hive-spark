win10 hadoop hive spark 配置流程<br>
一、解压安装包jdk到D盘，不要解压到其他的盘(这个我会通过U盘传给你们，里面的内容都是配置好的，无需再动)<br>
二、配置jdk<br>
    &nbsp;1. 打开jdk文件目录下的jdk1.8.0，将其添加到系统的环境变量里面去，具体步骤：<br>
        &nbsp;&nbsp;(1) 打开高级系统设置->环境变量->在系统变量里面点击新建，内容如下:<br>
        ![Image text](https://gitlab.com/-/ide/project/githubqzk/win10-hadoop-hive-spark/tree/master/-/1.png/)<br>
        &nbsp;&nbsp;(2) 在系统变量里面找到Path,点击编辑，所添加的内容如下:<br>
        ![Image text](https://gitlab.com/-/ide/project/githubqzk/win10-hadoop-hive-spark/tree/master/-/2.png/)<br>
        &nbsp;&nbsp;(3) 点击确定<br>
        &nbsp;&nbsp;(4) 打开cmd 命令行，在命令行里面输入java -version,反馈出如下结果便是成功：<br>
        ![Image text](https://gitlab.com/-/ide/project/githubqzk/win10-hadoop-hive-spark/tree/master/-/3.png/)<br>
三、配置hadoop
    &nbsp;1.hadoop所在文件夹为D:\jdk\hadoop-2.7.2，将其添加到系统的环境变量里面去，具体步骤如下:<br>
    &nbsp;&nbsp;(1) 打开高级系统设置->环境变量->在系统变量里面点击新建，内容如下:<br>
    ![Image text](https://gitlab.com/-/ide/project/githubqzk/win10-hadoop-hive-spark/tree/master/-/4.png/)<br>
    &nbsp;&nbsp;(2) 在系统变量里面找到Path,点击编辑，所添加的内容如下:<br>
    ![Image text](https://gitlab.com/-/ide/project/githubqzk/win10-hadoop-hive-spark/tree/master/-/5.png/)<br>
    &nbsp;&nbsp;(3) 点击确定
    &nbsp;&nbsp;(4) 打开cmd 命令行，在命令行里面输入java -version,反馈出如下结果便是成功：<br>
    ![Image text](https://gitlab.com/-/ide/project/githubqzk/win10-hadoop-hive-spark/tree/master/-/6.png/)<br>

        
        
